<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$trainerID = $_POST['trainerID'];

$sql = "SELECT * FROM trainer WHERE Trainer_ID=$trainerID";

$result = $link->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Ausbilder*in: " . $row["FirstName"]. " " . $row["LastName"] . " <br>". 
        "Vorname lt. Ausweis: " . $row["FirstNameInDoc"]. "<br>" .
        "E-Mail: "  . $row["EMail"]. "<br>" . "<br>";

        ?>
        <form method="post" action="UpdateTrainerDataAdmin2.php">
            <input type="hidden" name="trainerID" value="<?php echo $trainerID; ?>">

            <label for="firstname">Vorname:</label>
            <input type="text" name="firstname"><br><br>

            <label for="firstnamedoc">Vorname lt. Ausweis:</label>
            <input type="text" name="firstnamedoc"><br><br>
            <p>Sollte der Name im Ausweisdokument vom eigenen Rufnamen abweichen, bitte tragen Sie hier den Vornamen lt. Ausweisdokument ein. Andernfalls tragen Sie hier denselben Namen ein.</p>

            <label for="lastname">Nachname:</label>
            <input type="text" name="lastname"><br><br>

            <label for="email">E-Mail:</label>
            <input type="text" name="email"><br><br>

            <input type="submit" value="Update">
        </form>
        <?php
    }
} else {
    echo "0 results";
} 

include("Templates/Footer.php");
?>
