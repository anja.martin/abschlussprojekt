<?php 
include('Templates/Head.php'); 
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

// Variable um Azubi anzugeben
$trainerID = $_POST['trainerID']; 

$sql = "SELECT * FROM Trainer, Team WHERE Trainer_ID = $trainerID AND Trainer.Team_ID=Team.Team_ID"; 

$result = $link->query($sql);

if ($result->num_rows > 0) {
    
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Trainer: " . $row["FirstName"]. " " . $row["LastName"] . "<br><br>". " " . "Team: " . $row["Team"] . " " . "Department: " . $row["Department"] . "<br><br>";

        
        ?>
        <form method="post" action="DeleteTrainerAdmin.php">
            <input type="hidden" name="trainerID" value="<?php echo $trainerID; ?>">
            <input type="submit" value="Ausbilder*in löschen" name="Antrag">
        </form>

        <form method="post" action="UpdateTrainerDataAdmin1.php">            
            <input type="hidden" name="trainerID" value="<?php echo $trainerID; ?>">
            <input type="submit" value="Profilinformation bearbeiten" name="Antrag">
        </form>
        <?php
    }
} else {
    echo "0 results";
}

$link->close();

include('Templates/Footer.php');
?>
