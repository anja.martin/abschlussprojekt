<?php 
include('Templates/Head.php'); 
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

// Variable um Azubi anzugeben
$a = $_POST['azubiID']; 

$sql = "SELECT * FROM Trainee WHERE Trainee_ID = $a"; 
$result = $link->query($sql);

if ($result->num_rows > 0) {
    // Lesen Azubidaten
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Trainee: " . $row["FirstName"]. " " . $row["LastName"] . " <br>" . "Available Vacation Days: " . $row["VacationDays"]. "<br>" . "<br>";
        ?>
        <form method="post" action="CreateVacationRequest1.php">
                        
            <input type="hidden" name="traineeID" value="<?php echo $a; ?>">
            <input type="submit" value="Urlaubsantrag stellen" name="Antrag">
        </form>

        <form method="post" action="UpdateTraineeData1.php">            
            <input type="hidden" name="traineeID" value="<?php echo $a; ?>">
            <input type="submit" value="Profilinformation bearbeiten" name="Antrag">
        </form>
        <?php
    }
} else {
    echo "0 results";
}

$sql2 = "SELECT * FROM VacationTime WHERE Trainee_ID = $a"; 

$result2 = $link->query($sql2);

if ($result2->num_rows > 0) {
    // Urlaubsdaten ausgeben
    while($row = $result2->fetch_assoc()) {
        echo "Urlaub vom: " . $row["VacationStartDate"]. " " . "urlaub bis: " . $row["VacationEndDate"]. " " . "Beantragte Urlaubstage: "  . $row["VacationDays"]. " " . "Status Ausbilder*in: " . $row["Status_Trainer"]. " " . "Status IT-Ausbildungskoordination: " . $row["Status_Administrator"] . "<br>";
    }
} else {
    echo "0 results";
}

$link->close();

include('Templates/Footer.php');
?>
