<?php 
include('Templates/Head.php'); 
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$a = $_POST['traineeID']; 

$sql = "SELECT * FROM Trainee WHERE Trainee_ID = $a"; 

$result = $link->query($sql);

if ($result->num_rows > 0) {
    
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Azubi: " . $row["FirstName"]. " " . $row["LastName"] . " <br>" . "Verfügbare Urlaubstage: " . $row["VacationDays"]. "<br>" . "<br>";
        ?>
        <form method="post" action="DeleteAzubiAdmin.php">
            <input type="hidden" name="traineeID" value="<?php echo $a; ?>">
            <input type="submit" value="Azubi löschen" name="Antrag">
        </form>

        <form method="post" action="UpdateTraineeData1.php">            
            <input type="hidden" name="traineeID" value="<?php echo $a; ?>">
            <input type="submit" value="Profilinformation bearbeiten" name="Antrag">
        </form>
        <?php
    }
} else {
    echo "0 results";
}

$sql2 = "SELECT * FROM VacationTime WHERE Trainee_ID = $a"; 

$result2 = $link->query($sql2);

if ($result2->num_rows > 0) {
    
    while($row = $result2->fetch_assoc()) {
        echo "Urlaub von: " . $row["VacationStartDate"]. " " . "Urlaub bis: " . $row["VacationEndDate"]. " " . "Beantrage Urlaubstage: "  . $row["VacationDays"]. " " . "Status Ausbilder*in: " . $row["Status_Trainer"]. " " . "Status IT-Ausbildungskoordination: " . $row["Status_Administrator"] . "<br>";

        
        ?>
        <form method="post" action="UpdateStatusAdmin.php">
            <input type="hidden" name="vacationID" value="<?php echo $row["VacationTime_ID"]; ?>">
            <input type="submit" value="Urlaubsantrag bewilligen" name="Antrag">
        </form>
        <?php
    }
} else {
    echo "0 results";
}

$link->close();

include('Templates/Footer.php');
?>
