<?php
    include("Templates/Head.php");
    include("Templates/TopNavBar.php");
    include("Templates/titleopen.php");
    ?>
    Ausbilder*in anlegen
<?php
    include("Templates/titleclose.php");
    include("Templates/DBConnection.php");
?>

<form method="post" action="CreateTrainerData2.php">

    <label for="firstname">Vorname:</label>
    <input type="text" name="firstname"><br><br>

    <label for="firstnamedoc">Vorname lt. Ausweis:</label>
    <input type="text" name="firstnamedoc"><br><br>
    <p>Sollte der Name im Ausweisdokument vom eigenen Rufnamen abweichen, bitte tragen Sie hier den Vornamen lt. Ausweisdokument ein. Andernfalls tragen Sie hier denselben Namen ein.</p>

    <label for="lastname">Nachname:</label>
    <input type="text" name="lastname"><br><br>

    <label for="email">E-Mail:</label>
    <input type="text" name="email"><br><br>

    <label for="team">Team:</label>
    <select name="team">
    <?php
    // Hier werden die vorhandenen Teams/ Bereich aus der DB geladen
    $sqlTeams = "SELECT Team_ID, Team, Department FROM team";
    $resultTeams = mysqli_query($link, $sqlTeams);
    if (mysqli_num_rows($resultTeams) > 0) {
        while ($rowTeam = mysqli_fetch_assoc($resultTeams)) {
            echo "<option value='" . $rowTeam['Team_ID'] . "'>" . $rowTeam['Team'] . " (Bereich: " . $rowTeam['Department'] . ")" . "</option>";
        }
    }
    ?>
</select><br><br>

    <input type="submit" value="Ausbilder*in anlegen">
</form>

<?php
    include("Templates/Footer.php");
?>