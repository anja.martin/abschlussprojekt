<?php
include('Templates/Head.php'); 
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $vacationID = $_POST['vacationID'];

    // Aktualisierung der Ausbilder*inendaten
    $updateSQL = "UPDATE VacationTime SET Status_Trainer = true WHERE VacationTime_ID = $vacationID";

    if (mysqli_query($link, $updateSQL)) {
        echo "Approval granted successfully";

        // Überprüfung, ob Status_Trainer 1 und Status_Administrator 0
        $checkSQL = "SELECT * FROM VacationTime WHERE VacationTime_ID = $vacationID AND Status_Administrator = 0 AND Status_Trainer = 1";
        $result = mysqli_query($link, $checkSQL);
        $row = mysqli_fetch_assoc($result);
        if ($row) {
            // Aktualisierung Status_Trainer 
            $updateTraineeSQL = "UPDATE Trainee SET VacationDays = VacationDays - (SELECT VacationDays FROM VacationTime WHERE VacationTime_ID = $vacationID) WHERE Trainee_ID = {$row['Trainee_ID']}";
            if (mysqli_query($link, $updateTraineeSQL)) {
                echo "Vacation days updated successfully for Trainee ID: {$row['Trainee_ID']}";
            } else {
                echo "Error updating vacation days for Trainee ID: {$row['Trainee_ID']}";
            }
        } else {
            // Überprüfung Status_Administrator und Status_Trainer
            $checkSQL = "SELECT * FROM VacationTime WHERE VacationTime_ID = $vacationID AND Status_Administrator = 1";
            $result = mysqli_query($link, $checkSQL);
            $row = mysqli_fetch_assoc($result);
            if ($row['Status_Trainer'] == 1 && $row['Status_Administrator'] == 1) {
                // Überprüfung Status_Both
                if ($row['Status_Both'] == 0) {
                    // Aktualisierung Status_Both 1
                    $updateBothSQL = "UPDATE VacationTime SET Status_Both = 1 WHERE VacationTime_ID = $vacationID";
                    if (mysqli_query($link, $updateBothSQL)) {
                        // Lesen VacationDays aus Trainee TB
                        $traineeID = $row['Trainee_ID'];
                        $vacationDaysSQL = "SELECT VacationDays FROM Trainee WHERE Trainee_ID = $traineeID";
                        $vacationDaysResult = mysqli_query($link, $vacationDaysSQL);
                        $vacationDaysRow = mysqli_fetch_assoc($vacationDaysResult);
                        $currentVacationDays = $vacationDaysRow['VacationDays'];

                        // Lesen Urlaubstage aus VacationTime TB
                        $vacationDaysTaken = $row['VacationDays'];

                        // Berechnung übriger Urlaubstage
                        $remainingVacationDays = $currentVacationDays - $vacationDaysTaken;

                        // Eintragung aktualisierter Urlaubstage
                        $updateTraineeSQL = "UPDATE Trainee SET VacationDays = $remainingVacationDays WHERE Trainee_ID = $traineeID";
                        if (mysqli_query($link, $updateTraineeSQL)) {
                            echo "Vacation days updated successfully for Trainee ID: $traineeID";
                        } else {
                            echo "Error updating vacation days for Trainee ID: $traineeID";
                        }
                    } else {
                        echo "Error updating Status_Both: " . mysqli_error($link);
                    }
                } else {
                    echo "Vacation has already been approved";
                }
            }
        }
    } else {
        echo "Error updating approval status: " . mysqli_error($link);
    }
}

mysqli_close($link);
include('Templates/Footer.php');
?>
