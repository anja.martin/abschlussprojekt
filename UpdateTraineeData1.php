<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$traineeID = $_POST['traineeID'];

$sql = "SELECT * FROM trainee WHERE Trainee_ID=$traineeID";

$result = $link->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Trainee: " . $row["FirstName"]. " " . $row["LastName"] . " <br>". 
        "Ausbildungsjahr: " . $row["Year"]. "<br>" .
        "Ausbilder*in: " . $row["Trainer_ID"]. "<br>" . 
        "E-Mail: "  . $row["EMail"]. "<br>" . "<br>" .
        "Verfügbare Urlaubstage: " . $row["VacationDays"]. "<br>" ;
    }
} else {
    echo "0 results";
} 
?>

<form method="post" action="UpdateTraineeData2.php">
    <input type="hidden" name="traineeID" value="<?php echo $traineeID; ?>">

    <label for="firstname">Vorname:</label>
    <input type="text" name="firstname"><br><br>

    <label for="firstnamedoc">Vorname im Ausweis:</label>
    <input type="text" name="firstnamedoc"><br><br>
    <p>Sollte der Name im Ausweisdokument vom eigenen Rufnamen abweichen, bitte tragen Sie hier den Vornamen lt. Ausweisdokument ein. Andernfalls tragen Sie hier denselben Namen ein.</p>

    <label for="lastname">Nachname:</label>
    <input type="text" name="lastname"><br><br>

    <label for="email">E-Mail:</label>
    <input type="text" name="email"><br><br>

    <label for="gender">Gender:</label>
    <select name="gender">
        <option value="female">Weiblich</option>
        <option value="male">Männlich</option>
        <option value="other">Divers</option>
    </select><br><br>


    <label for="trainer">Ausbilder*in:</label>
    <select name="trainer">
        <?php
        // Ausbilder*innen werden hier aus der DB geladen
        $sqlTrainers = "SELECT Trainer_ID, FirstName, LastName FROM trainer";
        $resultTrainers = mysqli_query($link, $sqlTrainers);
        if (mysqli_num_rows($resultTrainers) > 0) {
            while ($rowTrainer = mysqli_fetch_assoc($resultTrainers)) {
                echo "<option value='" . $rowTrainer['Trainer_ID'] . "'>" . $rowTrainer['FirstName'] . " " . $rowTrainer['LastName'] . "</option>";
            }
        }
        ?>
    </select><br><br>

    <!-- Add a field for selecting the team -->
<label for="team">Team:</label>
<select name="team">
    <?php
    // Fetch existing teams from the database
    $sqlTeams = "SELECT Team_ID, Team, Department FROM team";
    $resultTeams = mysqli_query($link, $sqlTeams);
    if (mysqli_num_rows($resultTeams) > 0) {
        while ($rowTeam = mysqli_fetch_assoc($resultTeams)) {
            echo "<option value='" . $rowTeam['Team_ID'] . "'>" . $rowTeam['Team'] . " (Bereich: " . $rowTeam['Department'] . ")" . "</option>";
        }
    }
    ?>
</select><br><br>


    <input type="submit" value="Update">
</form>

<?php
include("Templates/Footer.php");
?>
