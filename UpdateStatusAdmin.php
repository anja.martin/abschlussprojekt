<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$vacationID = $_POST['vacationID'];

$sql = "UPDATE VacationTime SET Status_Administrator = TRUE WHERE VacationTime_ID = $vacationID";

if (mysqli_query($link, $sql)) {
    // Überprüfung Status_Administrator und Status_Trainer auf 1
    $checkSQL = "SELECT * FROM VacationTime WHERE VacationTime_ID = $vacationID AND Status_Trainer = 1 AND Status_Administrator = 1 AND Status_Both = 0";
    $result = mysqli_query($link, $checkSQL);
    $row = mysqli_fetch_assoc($result);

    if ($row) {
        // Überprüfung Status_Both 1
        $updateBothSQL = "UPDATE VacationTime SET Status_Both = 1 WHERE VacationTime_ID = $vacationID";
        mysqli_query($link, $updateBothSQL);

        // Variablen zur Datenweitergabe
        $traineeID = $row['Trainee_ID'];
        $vacationDays = $row['VacationDays'];

        // Lesen aktuelle Urlaubstage
        $vacationDaysSQL = "SELECT VacationDays FROM Trainee WHERE Trainee_ID = $traineeID";
        $vacationDaysResult = mysqli_query($link, $vacationDaysSQL);
        $vacationDaysRow = mysqli_fetch_assoc($vacationDaysResult);
        $currentVacationDays = $vacationDaysRow['VacationDays'];

        // Berechnung übrige Urlaubstage
        $remainingVacationDays = $currentVacationDays - $vacationDays;

        // Aktualisierung Urlaubstage in VacationTime TB
        $updateTraineeSQL = "UPDATE Trainee SET VacationDays = $remainingVacationDays WHERE Trainee_ID = $traineeID";
        if (mysqli_query($link, $updateTraineeSQL)) {
            echo "Urlaubstage wurden erfolgreich aktualisiert.";
        } else {
            echo "Urlaubstage konnten nicht aktualisiert werden.";
        }
    } else {
        echo "IT-Ausbildungskoordination erfolgreich bewilligt.";
    }
} else {
    // Überprüfung, ob Status_Trainer 0 und Status_Administrator 0
    $checkSQL = "SELECT * FROM VacationTime WHERE VacationTime_ID = $vacationID AND Status_Trainer = 0 AND Status_Administrator = 0";
    $result = mysqli_query($link, $checkSQL);
    $row = mysqli_fetch_assoc($result);
    if ($row) {
        // Aktualisierung Status_Administrator 1
        $updateAdminSQL = "UPDATE VacationTime SET Status_Administrator = 1 WHERE VacationTime_ID = $vacationID";
        if (mysqli_query($link, $updateAdminSQL)) {
            echo "Urlaubsantrag bewilligt.";
        } else {
            echo "Error bei der Stellung des Urlaubsantrages: " . mysqli_error($link);
        }
    } else {
        echo "Error: " . mysqli_error($link);
    }
}

include("Templates/Footer.php");
?>
