<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$traineeID = $_POST['traineeID'];

$sql = "SELECT * FROM trainee WHERE Trainee_ID=$traineeID";

$result = $link->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Trainee: " . $row["FirstName"]. " " . $row["LastName"] . " <br>". 
        "Year: " . $row["Year"]. "<br>" .
        "Trainer: " . $row["Trainer_ID"]. "<br>" . 
        "E-Mail: "  . $row["EMail"]. "<br>" . "<br>" .
        "Available Holiday Days: " . $row["VacationDays"]. "<br>" ;
    }
} else {
    echo "0 results";
} 
?>

<form method="post" action="UpdateTraineeData2.php">
    <input type="hidden" name="traineeID" value="<?php echo $traineeID; ?>">

    <label for="firstname">First Name:</label>
    <input type="text" name="firstname"><br><br>

    <label for="firstnamedoc">First Name as per ID Document:</label>
    <input type="text" name="firstnamedoc"><br><br>
    <p>If the name used in the ID document differs from the regular usage, please provide it here; otherwise, enter the same name as in the First Name field.</p>

    <label for="lastname">Last Name:</label>
    <input type="text" name="lastname"><br><br>

    <label for="email">E-Mail:</label>
    <input type="text" name="email"><br><br>

    <label for="year">Year:</label>
    <select name="year">
        <option value="1">1st Year</option>
        <option value="2">2nd Year</option>
        <option value="3">3rd Year</option>
    </select><br><br>

    <label for="gender">Gender:</label>
    <select name="gender">
        <option value="female">Female</option>
        <option value="male">Male</option>
        <option value="other">Other</option>
        <option value="n.a.">Not specified</option>
    </select><br><br>

    <label for="urlaubstage">Holiday Days:</label>
    <input type="number" name="holidaydays" min="-1"><br><br>

    <input type="submit" value="Update">
</form>

<?php
include("Templates/Footer.php");
?>
