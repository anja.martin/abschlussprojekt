<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$traineeID = $_POST['traineeID'];

$updates = array();
if (!empty($_POST['firstname'])) {
    $updates[] = "FirstName = '$_POST[firstname]'";
}
if (!empty($_POST['firstnamedoc'])) {
    $updates[] = "FirstNameInDoc = '$_POST[firstnamedoc]'";
}
if (!empty($_POST['lastname'])) {
    $updates[] = "LastName = '$_POST[lastname]'";
}

if (!empty($_POST['email'])) {
    $updates[] = "EMail = '$_POST[email]'";
}
if (!empty($_POST['gender'])) {
    $updates[] = "Gender = '$_POST[gender]'";
}
if (!empty($_POST['department'])) {
    $updates[] = "Team_ID = '$_POST[department]'";
}
if (!empty($_POST['trainer'])) {
    $updates[] = "Trainer_ID = '$_POST[trainer]'";
}
if (!empty($_POST['team'])) {
    $updates[] = "Team_ID = '$_POST[team]'";
}

$updateFields = implode(", ", $updates);

$sql = "UPDATE trainee SET $updateFields WHERE Trainee_ID=$traineeID";

if (mysqli_query($link, $sql)) {
    echo "Information erfolgreich aktualisiert.";
} else {
    echo "Error bei der Aktualisierung in der Datenbank: " . mysqli_error($link);
}

include("Templates/Footer.php");
?>
