<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$sql = "SELECT * FROM Trainer";

$result = $link->query($sql);

if ($result->num_rows > 0) {
   // Lesen der Ausbilder*innendaten
   while($row = $result->fetch_assoc()) {
       echo "Trainer: " . $row["FirstName"]. " " . $row["LastName"]. "<br>";
       ?>
       <form method="post" action="ProfileTrainer.php">
           <?php $trainerID = $row["Trainer_ID"]; ?>
           <input type="hidden" name="trainerID" value="<?php echo $trainerID; ?>">
           <input type="submit" value="Profil" name="Profile">
       </form>
       <?php
   }
} else {
   echo "0 results";
}
$link->close();

include("Templates/Footer.php");
?>
