<?php
    include("Templates/Head.php");
    include("Templates/TopNavBar.php");
    include("Templates/titleopen.php");
    ?>
    Azubi anlegen
<?php
    include("Templates/titleclose.php");
    include("Templates/DBConnection.php");
?>



<form method="post" action="CreateAzubiData2.php">

    <label for="firstname">Vorname:</label>
    <input type="text" name="firstname"><br><br>

    <label for="firstnamedoc">Vorname lt. Ausweis:</label>
    <input type="text" name="firstnamedoc"><br><br>
    <p>Sollte der Name im Ausweisdokument vom eigenen Rufnamen abweichen, bitte tragen Sie hier den Vornamen lt. Ausweisdokument ein. Andernfalls tragen Sie hier denselben Namen ein.</p>

    <label for="lastname">Nachname:</label>
    <input type="text" name="lastname"><br><br>

    <label for="bday">Geburtsdatum:</label>
    <input type="date" name="bday"><br><br>

    <label for="year">Ausbildungsjahr:</label>
    <select name="year">
        <option value="1">Erstes Jahr</option>
        <option value="2">Zweites Jahr</option>
        <option value="3">Drittes Jahr</option>
    </select><br><br>

    <label for="gender">Gender:</label>
    <select name="gender">
        <option value="female">Weiblich</option>
        <option value="male">Männlich</option>
        <option value="other">Divers</option>
    </select><br><br>

    <label for="team">Team:</label>
    <select name="team">
    <?php
    // Teams/ Bereiche werden hier aus der DB ausgelesen
    $sqlTeams = "SELECT Team_ID, Team, Department FROM team";
    $resultTeams = mysqli_query($link, $sqlTeams);
    if (mysqli_num_rows($resultTeams) > 0) {
        while ($rowTeam = mysqli_fetch_assoc($resultTeams)) {
            echo "<option value='" . $rowTeam['Team_ID'] . "'>" . $rowTeam['Team'] . " (Bereich: " . $rowTeam['Department'] . ")" . "</option>";
        }
    }
    ?>
</select><br><br>

<label for="trainer">Ausbilder*in:</label>
    <select name="trainer">
        <?php
        // Ausbilder*innen werden hier aus der DB geladen
        $sqlTrainers = "SELECT Trainer_ID, FirstName, LastName FROM trainer";
        $resultTrainers = mysqli_query($link, $sqlTrainers);
        if (mysqli_num_rows($resultTrainers) > 0) {
            while ($rowTrainer = mysqli_fetch_assoc($resultTrainers)) {
                echo "<option value='" . $rowTrainer['Trainer_ID'] . "'>" . $rowTrainer['FirstName'] . " " . $rowTrainer['LastName'] . "</option>";
            }
        }
        ?>
    </select><br><br>

    <input type="submit" value="Azubi anlegen">
</form>

<?php
    include("Templates/Footer.php");
?>