<?php
    include("Templates/Head.php");
    include("Templates/TopNavBar.php");
    include("Templates/titleopen.php");
    ?>
    Urlaubsbeantragung
<?php
    include("Templates/titleclose.php");
    include("Templates/DBConnection.php");


$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$a = $_POST['traineeID'];

$link = mysqli_connect($servername, $username, $password, $dbname);

$vacation_start_date = $_POST['vacationStartDate'];
$vacation_end_date = $_POST['vacationEndDate'];
$vacation_days_requested = $_POST['vacationDaysRequested'];
$trainee_id = $_POST['traineeID']; // zur Weitertabe des Nutzers

// verfpgbare Urlaubstage anzeigen
$sql1 = "SELECT VacationDays FROM VacationTime WHERE Trainee_ID = $trainee_id";
$result1 = mysqli_query($link, $sql1);
if ($result1) {
    $row = mysqli_fetch_assoc($result1);
    $availableVacationDays = $row['VacationDays'];
    
    // Berechnung der verfügbaren Urlaubstage nachdem alles bewilligt wurde
    $remainingVacationDays = $availableVacationDays - $vacation_days_requested;

    // Einfügen aktuelle Urlaubstage
    $sql2 = "INSERT INTO VacationTime (Trainee_ID, VacationStartDate, VacationEndDate, VacationDays, Status_Trainer, Status_Administrator) 
             VALUES ($trainee_id, '$vacation_start_date', '$vacation_end_date', $vacation_days_requested, 0, 0)";
    $result2 = mysqli_query($link, $sql2);



    if ($result2) {
        echo "Urlaubsantrag eingereicht.";
    } else {
        echo "Error: " . mysqli_error($link);
    }
} else {
    echo "Error: " . mysqli_error($link);
}

mysqli_close($link);

include("Templates/Footer.php");

