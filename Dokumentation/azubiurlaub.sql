-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 26. Jun 2024 um 11:14
-- Server-Version: 10.4.24-MariaDB
-- PHP-Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `azubiurlaub`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `administrator`
--

CREATE TABLE `administrator` (
  `Administrator_ID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `FirstNameInDoc` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `EMail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `team`
--

CREATE TABLE `team` (
  `Team_ID` int(11) NOT NULL,
  `Team` varchar(128) NOT NULL,
  `Department` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `team`
--

INSERT INTO `team` (`Team_ID`, `Team`, `Department`) VALUES
(7, 'Content Management und Entwicklung', 'HRZ');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `trainee`
--

CREATE TABLE `trainee` (
  `Trainee_ID` int(11) NOT NULL,
  `FirstNameInDoc` varchar(255) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `BDay` date NOT NULL,
  `EMail` varchar(255) NOT NULL,
  `Team_ID` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `Gender` varchar(255) NOT NULL,
  `VacationDays` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `trainee`
--

INSERT INTO `trainee` (`Trainee_ID`, `FirstNameInDoc`, `FirstName`, `LastName`, `BDay`, `EMail`, `Team_ID`, `Year`, `Gender`, `VacationDays`, `Trainer_ID`) VALUES
(29, 'Anja', 'Anja', 'Martin', '2001-06-20', '', 7, 1, 'female', 30, 11);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `trainer`
--

CREATE TABLE `trainer` (
  `Trainer_ID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `FirstNameInDoc` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `EMail` varchar(255) NOT NULL,
  `Team_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `trainer`
--

INSERT INTO `trainer` (`Trainer_ID`, `FirstName`, `FirstNameInDoc`, `LastName`, `EMail`, `Team_ID`) VALUES
(11, 'Lena', 'Lena', 'Wiere', 'lena.wiere.imaginarymail.com', 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vacationprocess`
--

CREATE TABLE `vacationprocess` (
  `VacationProcess_ID` int(11) NOT NULL,
  `VacationDaysBefore` int(11) NOT NULL,
  `VacationDaysAfter` int(11) NOT NULL,
  `VacationTime_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vacationtime`
--

CREATE TABLE `vacationtime` (
  `VacationTime_ID` int(11) NOT NULL,
  `VacationDays` int(11) NOT NULL,
  `Status_Trainer` tinyint(1) NOT NULL,
  `Status_Administrator` tinyint(1) NOT NULL,
  `Status_Both` int(11) NOT NULL,
  `Trainee_ID` int(11) NOT NULL,
  `VacationStartDate` date DEFAULT NULL,
  `VacationEndDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `vacationtime`
--

INSERT INTO `vacationtime` (`VacationTime_ID`, `VacationDays`, `Status_Trainer`, `Status_Administrator`, `Status_Both`, `Trainee_ID`, `VacationStartDate`, `VacationEndDate`) VALUES
(26, 3, 0, 0, 0, 29, '2024-10-21', '2024-10-23');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`Administrator_ID`);

--
-- Indizes für die Tabelle `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`Team_ID`);

--
-- Indizes für die Tabelle `trainee`
--
ALTER TABLE `trainee`
  ADD PRIMARY KEY (`Trainee_ID`),
  ADD KEY `Team_ID` (`Team_ID`),
  ADD KEY `Trainer_ID` (`Trainer_ID`);

--
-- Indizes für die Tabelle `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`Trainer_ID`),
  ADD KEY `Team_ID` (`Team_ID`);

--
-- Indizes für die Tabelle `vacationprocess`
--
ALTER TABLE `vacationprocess`
  ADD PRIMARY KEY (`VacationProcess_ID`);

--
-- Indizes für die Tabelle `vacationtime`
--
ALTER TABLE `vacationtime`
  ADD PRIMARY KEY (`VacationTime_ID`),
  ADD KEY `Trainee_ID` (`Trainee_ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `administrator`
--
ALTER TABLE `administrator`
  MODIFY `Administrator_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `team`
--
ALTER TABLE `team`
  MODIFY `Team_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `trainee`
--
ALTER TABLE `trainee`
  MODIFY `Trainee_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT für Tabelle `trainer`
--
ALTER TABLE `trainer`
  MODIFY `Trainer_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT für Tabelle `vacationprocess`
--
ALTER TABLE `vacationprocess`
  MODIFY `VacationProcess_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `vacationtime`
--
ALTER TABLE `vacationtime`
  MODIFY `VacationTime_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `trainee`
--
ALTER TABLE `trainee`
  ADD CONSTRAINT `trainee_ibfk_1` FOREIGN KEY (`Team_ID`) REFERENCES `team` (`Team_ID`),
  ADD CONSTRAINT `trainee_ibfk_2` FOREIGN KEY (`Trainer_ID`) REFERENCES `trainer` (`Trainer_ID`);

--
-- Constraints der Tabelle `trainer`
--
ALTER TABLE `trainer`
  ADD CONSTRAINT `trainer_ibfk_1` FOREIGN KEY (`Team_ID`) REFERENCES `team` (`Team_ID`);

--
-- Constraints der Tabelle `vacationtime`
--
ALTER TABLE `vacationtime`
  ADD CONSTRAINT `vacationtime_ibfk_1` FOREIGN KEY (`Trainee_ID`) REFERENCES `trainee` (`Trainee_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
