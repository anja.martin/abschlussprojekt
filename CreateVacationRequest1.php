<?php
    include("Templates/Head.php");
    include("Templates/TopNavBar.php");
    include("Templates/titleopen.php");
    ?>
    Urlaubsbeantragung
<?php
    include("Templates/titleclose.php");
    include("Templates/DBConnection.php");


$a = $_POST['traineeID'];
?>

<form method="post" action="CreateVacationRequest2.php">
    
    <input type="hidden" name="traineeID" value="<?php echo $a; ?>">

    <label for="vacationStartDate">Urlaub vom:</label>
    <input type="date" name="vacationStartDate"><br><br>

    <label for="vacationEndDate">Urlaub bis:</label>
    <input type="date" name="vacationEndDate"><br><br>

    <label for="vacationDaysRequested">Beantragte Urlaubstage:</label>
    <input type="number" name="vacationDaysRequested" min="1"><br><br>

    <input type="submit" value="Urlaubsantrag abschicken">
</form>

<?php
include("Templates/Footer.php");
?>
