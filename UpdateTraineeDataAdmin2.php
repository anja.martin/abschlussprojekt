<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$traineeID = $_POST['traineeID'];

$updates = array();
if (!empty($_POST['firstname'])) {
    $updates[] = "FirstName = '$_POST[firstname]'";
}
if (!empty($_POST['firstnamedoc'])) {
    $updates[] = "FirstNameInDoc = '$_POST[firstnamedoc]'";
}
if (!empty($_POST['lastname'])) {
    $updates[] = "LastName = '$_POST[lastname]'";
}
if (!empty($_POST['email'])) {
    $updates[] = "EMail = '$_POST[email]'";
}
if (!empty($_POST['year'])) {
    $updates[] = "Year = '$_POST[year]'";
}
if (!empty($_POST['gender'])) {
    $updates[] = "Gender = '$_POST[gender]'";
}
if (!empty($_POST['holidaydays'])) {
    $updates[] = "HolidayDays = '$_POST[holidaydays]'";
}

$updateFields = implode(", ", $updates);

$sql = "UPDATE trainee SET $updateFields WHERE Trainee_ID=$traineeID";

if (mysqli_query($link, $sql)) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . mysqli_error($link);
}

include("Templates/Footer.php");
?>
