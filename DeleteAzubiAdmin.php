<?php
    include("Templates/Head.php");
    include("Templates/TopNavBar.php");
    include("Templates/titleopen.php");
    ?>
    Azubi löschen
<?php
    include("Templates/titleclose.php");
    include("Templates/DBConnection.php");


$servername = 'localhost';
$username = 'root';
$password = '';
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$traineeID = $_POST['traineeID'];

// SQL-Statement um Azubi aus DB zu löschen
$deleteTraineeSQL = "DELETE FROM Trainee WHERE Trainee_ID = $traineeID";
if (mysqli_query($link, $deleteTraineeSQL)) {
    echo "Azubi";
} else {
    echo "Error deleting trainee: " . mysqli_error($link);
}

// SQL-Statement um Urlaubsanträge mit Azubis aus DB zu löschen
$deleteVacationRequestsSQL = "DELETE FROM VacationTime WHERE Trainee_ID = $traineeID";
if (mysqli_query($link, $deleteVacationRequestsSQL)) {
    echo " und Urlaubstage erfolgreich gelöscht.";
} else {
    echo "Error deleting vacation requests: " . mysqli_error($link);
}

include("Templates/Footer.php");
?>
