<div class="row text-center">
    <div class="col-5"></div>
    <div class="col-2 h4 border-2 border-end border-start my-5 border-dark"> <a href="Index.php">Urlaubssystem</a></div>
    <div class="col-5"></div>
</div>

<ul class="nav row p-3 my-4 text-center" style="background-color: lightgrey;">
    <li class="col-2 p-0 text-black h5 border-end border-2 border-dark text-decoration-underline dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownProfile" data-bs-toggle="dropdown" aria-expanded="false">
            Profile
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownProfile">
            <li><a class="dropdown-item" href="OutputAzubiData.php">Azubis</a></li>
            <li><a class="dropdown-item" href="OutputTrainerData.php">Ausbilder*innen</a></li>
        </ul>
    </li>

    <li class="col-2 p-0 text-black h5 border-end border-2 border-dark text-decoration-underline dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownNewEntries" data-bs-toggle="dropdown" aria-expanded="false">
            Neue Einträge
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownNewEntries">
            <li><a class="dropdown-item" href="CreateAzubiData1.php">Azubis anlegen</a></li>
            <li><a class="dropdown-item" href="CreateTrainerData1.php">Ausbilder*innen anlegen</a></li>
            <li><a class="dropdown-item" href="CreateTeam1.php">Neue Abteilung</a></li>
        </ul>
    </li>

    <li class="col-2 p-0 text-black h5 border-end border-2 border-dark dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownAdministration" data-bs-toggle="dropdown" aria-expanded="false">
            Administration
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownAdministration">
            <li><a class="dropdown-item" href="OutputAzubiDataAdmin.php">Azubis</a></li>
            <li><a class="dropdown-item" href="OutputTrainerDataAdmin.php">Ausbilder*innen</a></li>
        </ul>
    </li>

    <li class="col-2"></li>
</ul>

<div class="container mb-5" style="background-color:lightgrey">
    <div class="row m-5">
        <div class="col-4"></div>
        
        