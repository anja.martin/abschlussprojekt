    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<!-- Added for container-->    
        <div class="col-4"></div>
    </div>
    

</body>
<footer>
  <section class="pt-4 pb-3 fixed-bottom" style="background:lightgrey">
        <div class="row justify-content-center">
            <div class="col-2 text-center">Kontakt</div>
            <div class="col-2 text-center border-start border-2 border-dark">Impressum</div>
            <div class="col-2 text-center border-start border-2 border-dark">Datenschutzerklärung</div>
        </div>
    </section>
</footer>

<!-- Anmerkung Anja: There may be troubel with the fixed bottom. For further information read here: https://getbootstrap.com/docs/5.3/helpers/position/#sticky-top -->