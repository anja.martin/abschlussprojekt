<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");
include("Templates/DBConnection.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$trainerID = $_POST['trainerID'];

$sql = "SELECT * FROM Trainer WHERE Trainer_ID=$trainerID";

$result = $link->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Trainer: " . $row["FirstName"]. " " . $row["LastName"] . "<br>" . "E-Mail: " . $row["EMail"] . "<br>" . "<br>";
    }
} else {
    echo "0 results";
} 
?>

<form method="post" action="UpdateTrainerData2.php">
    <input type="hidden" name="trainerID" value="<?php echo $trainerID; ?>">

    <label for="firstname">First Name:</label>
    <input type="text" name="firstname"><br><br>

    <label for="lastname">Last Name:</label>
    <input type="text" name="lastname"><br><br>

    <label for="email">E-Mail:</label>
    <input type="text" name="email"><br><br>

    <label for="team">Team:</label>
        <select name="team">
            <?php
            // Anzeige der Teams/ Bereiche aus DB
            $sqlTeams = "SELECT Team_ID, Team, Department FROM team";
            $resultTeams = mysqli_query($link, $sqlTeams);
            if (mysqli_num_rows($resultTeams) > 0) {
                while ($rowTeam = mysqli_fetch_assoc($resultTeams)) {
                    echo "<option value='" . $rowTeam['Team_ID'] . "'>" . $rowTeam['Team'] . " (Department: " . $rowTeam['Department'] . ")" . "</option>";
                }
            }
            ?>
</select><br><br>

    <input type="submit" value="Update">
</form>

<?php
include("Templates/Footer.php");
?>
