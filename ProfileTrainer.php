<?php 
include('Templates/Head.php'); 
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

// Variable um Ausbilder*n anzugeben
$a = $_POST['trainerID']; 

$sql = "SELECT * FROM Trainee WHERE Trainer_ID = $a"; // Assuming Trainer_ID is the foreign key in the Trainee table

$result = $link->query($sql);

if ($result->num_rows > 0) {
    // Lesen Ausbilder*innendaten
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "Azubi: " . $row["FirstName"]. " " . $row["LastName"] . " <br>" . "Verfügbare Urlaubstage: " . $row["VacationDays"]. "<br>" . "<br>";
        
        ?>
        <form method="post" action="UpdateTrainerData1.php">            
            <input type="hidden" name="trainerID" value="<?php echo $a; ?>">
            <input type="submit" value="Profilinformation bearbeiten" name="Antrag">
        </form>
        <?php

        // Hier werden Urlaubsanträge wiedergegeben
        $traineeID = $row["Trainee_ID"];
        $vacationSQL = "SELECT * FROM VacationTime WHERE Trainee_ID = $traineeID";
        $vacationResult = $link->query($vacationSQL);

        if ($vacationResult->num_rows > 0) {
             // Div soll scrollbar sein, damit alle Anfragen angezeigt werden, unabhängig von der Größe des Fensters
            echo '<div style="height: 200px; overflow: auto;">';
            
            // Ausgabe urlaubsanträge Inhalte
            while ($vacationRow = $vacationResult->fetch_assoc()) {
                echo "Urlaub von: " . $vacationRow["VacationStartDate"]. "<br>";
                echo "Urlaub bis: " . $vacationRow["VacationEndDate"]. "<br>";
                echo "Beantragte Urlaubstage: "  . $vacationRow["VacationDays"]. "<br>";
                echo "Status Ausbilder*in: " . $vacationRow["Status_Trainer"]. "<br>";
                echo "Status IT-Ausbildungskoordination: " . $vacationRow["Status_Administrator"]. "<br>";
                
                echo "<form method='post' action='UpdateStatusTrainer.php'>";
                echo "<input type='hidden' name='vacationID' value='" . $vacationRow["VacationTime_ID"] . "'>";
                echo "<input type='submit' value='Urlaubsantrag bewilligen' name='Antrag'>";
                echo "</form>";
                
                echo "<br>";
            }
            
            echo '</div>';
        } else {
            echo "Diese*r Azubi hat noch keinen Urlaubsantrag eingereicht.";
        }
        

    }
} else {
    echo "0 results";
}

$link->close();

include('Templates/Footer.php');
?>
