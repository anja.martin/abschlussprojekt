<?php
include("Templates/Head.php");
include("Templates/TopNavBar.php");

$servername = 'localhost'; 
$username = 'root'; 
$password = ''; 
$dbname = 'azubiurlaub';

$link = mysqli_connect($servername, $username, $password, $dbname);

$sql = "SELECT * FROM Trainee";
/** Hier noch kein weiteres Read/Update etc. einfügen, sondern auf Profil weiterleiten 
 * und dort einfach die Option zu Update, Delete etc. geben. -> Übersichtlicher!!! */

$result = $link->query($sql);

if ($result->num_rows > 0) {
   // Lesen der Azubidaten
   while($row = $result->fetch_assoc()) {
       echo "Azubi: " . $row["FirstName"]. " " . $row["LastName"]. "<br>";
       ?>
       <form method="post" action="ProfileAzubiAdmin.php">
           <?php $a= $row["Trainee_ID"]; ?>
           <input type="hidden" name="traineeID" <?php echo " value=\"$row[Trainee_ID]\""?>>
           <input type="submit" value="Profil" name="Profile">
       </form>
       <?php
   }
} else {
   echo "0 results";
}
$link->close();

include("Templates/Footer.php");
?>